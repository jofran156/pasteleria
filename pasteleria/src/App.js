import "./App.css";

import React from "react";

import Filter from "./components/Filter/Filter";

function App() {
  return (
    <div className="App">
      <Filter />
    </div>
  );
}

export default App;
