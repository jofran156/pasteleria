import React from "react";

import "./Card.css";

function Card(props) {
  const { name, imageUrl, description, price, comboPrice, toppingType, size } = props;

 
  function capitalizarLetra(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  function formatearPrecio(number) {
    return new Intl.NumberFormat("es-MX", {
      style: "currency",
      currency: "MXN",
      minimumFractionDigits: 2,
    }).format(number);
  }

  function topping(type) {
    if (type === "0") {
      return "Fondeau";
    } else if (type === "1") {
      return "Betun italiano";
    } else if (type === "2") return "Chantilly";
  }

  return (
    <div className="card">
      <div className="card__img">
        <img src={imageUrl} alt="" />
      </div>
      <div className="card__info">
        <h1 className="card__title">{capitalizarLetra(name)}</h1>
        <div className="card__type">
            <p>Tamaño:  {size}</p>
            <p>Topping:  {topping(toppingType)}</p>
        </div>
        <p className="card__text">{description}</p>
        <div className="card__price">
          <div className="">
            <span>PASTEL</span>
            <p>Combo fiesta</p>
            <span>{formatearPrecio(price)}</span>
            <p className="card__button">SELECT</p>
          </div>
          <div className="">
            <span>PAQUETE</span>
            <p>15-17 Personas</p>
            <span>{formatearPrecio(comboPrice)}</span>
            <p className="card__button">SELECT</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Card;
