import React, { useState, useEffect } from "react";
import data from "../../data/data";
import Card from "../Card/Card";



function Filter() {
  const [dataPasteles, setDataPasteles] = useState([]);
  const [userSearchData, setUserSearchData] = useState([]);
  const [name, setName] = useState("");
  const [price, setPrice] = useState("");  
  const [size, setSize] = useState("");
  const [toppingType, setToppingType] = useState("");
  const [comboPrice, setComboPrice] = useState("");

//Obteniendo data
  useEffect(() => {
    setDataPasteles(data);
    setUserSearchData(data);
  

    return () => {
   
    };
  }, []);
//Función de busqueda
  const handleSearch = (e) => {
    e.preventDefault();
    const newData = 
    dataPasteles    
    .filter((elemento) =>{
      if(elemento.name.toString().toLowerCase().includes(name.toLowerCase())){
       return elemento
      }
    } )
    

    .filter((b)=>b.size=== (size==="" ? b.size:size))   

    .filter((c)=>c.toppingType===(toppingType==="" ? c.toppingType:toppingType)) 

   

    .filter(p => p.price<=(parseInt(price)<501?
      parseInt(price):p.price))
     

  
    //(parseInt(price)<451?parseInt(price):p.price)

    .filter(x =>x.comboPrice<=(parseInt(comboPrice,10)<501?
    parseInt(comboPrice,10)
    :parseInt(comboPrice,10)<1001?
    parseInt(comboPrice,10):x.comboPrice))
 
  
     
    console.log(newData);


      
    setUserSearchData(newData);
  };
 
//Componente de para tarjeta
  const pasteles = userSearchData.map((pastel,index) => (
    <Card
    key={index}
      name={pastel.name}
      imageUrl={pastel.imageUrl}
      description={pastel.description}
      price={pastel.price}
      comboPrice={pastel.comboPrice}
      toppingType={pastel.toppingType}
      size={pastel.size}
    />
  ));

  return (
    <div className="form">
      <form onSubmit={handleSearch}>
        <input
          type="text"
          name="name"
          placeholder="Nombre"
          onChange={(e) => setName(e.target.value)}
        ></input>

        <select onChange={(e)=>{setPrice(e.target.value)}}>
          <option value=""> PRECIO </option>
          <option value="500">Menos de 500</option>
        
         
        </select>
        
        <select onChange={(e)=>{setComboPrice(e.target.value)}}>
          <option value=""> PRECIO COMBO </option>
          <option value="500">Menos de 500</option>
          <option value="1000">Menos de 1000</option>
          <option value="1500">Menos de 1500</option>
         
        </select>




        <select onChange={(e)=>{setSize(e.target.value)}}>
          <option value=""> TAMAÑO </option>
          <option value="G">Grande</option>
          <option value="M">Mediano</option>
          <option value="S">Pequeño</option>
        </select>
        
        <select onChange={(e)=>{setToppingType(e.target.value)}}>
          <option value=""> TOPPING </option>
          <option value="0">Fondeau</option>
          <option value="1">Betun italiano</option>
          <option value="2">Chantilly</option>
        </select>


        
        <button type="submit">Buscar</button>
      </form>
      {userSearchData && userSearchData.length > 0 ? pasteles : "No tenemos ese producto"}
    </div>
  );
}

export default Filter;
